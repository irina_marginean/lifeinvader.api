﻿using System.ComponentModel.DataAnnotations;

namespace LifeInvader.API.DTOs
{
    public class UserForRegisterDTO
    {
        [Required]
        public string Username{ get; set; }

        [Required]
        [StringLength(8, MinimumLength = 3, ErrorMessage = "Your password must have at least 3 and at most 8 characters!")]
        public string Password{ get; set; }
    }
}
