﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LifeInvader.API.Models;
using Microsoft.EntityFrameworkCore;

namespace LifeInvader.API.Data
{
    public class UserRepository : IUserRepository
    {
        private readonly DataContext dataContext;

        public UserRepository(DataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public void Add<T>(T entity) where T : class
        {
            dataContext.Add(entity);
        }

        public void Delete<T>(T entity) where T : class
        {
            dataContext.Remove(entity);
        }

        public async Task<bool> SaveAll()
        {
            return await dataContext.SaveChangesAsync() > 0;
        }

        public async Task<IEnumerable<User>> GetUsers()
        {
            var users = await dataContext.
                Users.
                Include(x => x.Photos).
                ToListAsync();

            return users;
        }

        public async Task<User> GetUser(int id)
        {
            var user = await dataContext.
                Users.
                Include(x => x.Photos).
                FirstOrDefaultAsync(y => y.Id == id);

            return user;
        }
    }
}
